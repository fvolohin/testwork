# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class CommentsConfig(AppConfig):
    name = 'testwork.comments'

    def ready(self):
        import testwork.comments.signals
