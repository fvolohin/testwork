from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from testwork.comments.models import Comment, CommentHistory, ChangeType


@receiver(post_save, sender=Comment)
def add_history_changed(sender, instance, created, **kwargs):
    if not created:
        change_type = ChangeType.objects.get(name='edit')
        history = CommentHistory(comment=instance.id, change_type=change_type)
        history.save()


@receiver(post_delete, sender=Comment)
def add_history_deleted(sender, instance, **kwargs):
    change_type = ChangeType.objects.get(name='delete')
    history = CommentHistory(comment=instance.id, change_type=change_type)
    history.save()
