# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings

from django.shortcuts import render
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.core.serializers import serialize
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseBadRequest, JsonResponse

from django_q.tasks import async, result

from testwork.comments.models import Comment, CommentHistory, EntityType
from testwork.comments.tasks import fetch_comments


def create_comment(request, entity_type_id, entity_id,  user_id, content):
    allowed_entities = ['blog_page_id', 'user_page_id', 'comment_id']
    try:
        entity_type = EntityType.objects.get(id=entity_type_id)
        user = User.objects.get(id=user_id)
    except EntityType.DoesNotExist:
        return HttpResponseNotFound('Entity type does not exist')
    except User.DoesNotExist:
        return HttpResponseNotFound('User does not exist')

    if entity_type.name not in allowed_entities:
        return HttpResponseBadRequest('Entity type not allowed')

    if entity_type.name == 'comment_id':
        try:
            parent_comment = Comment.objects.get(id=entity_id)
        except Comment.DoesNotExist:
            return HttpResponseNotFound('Parent comment does not exist')
    else:
        # try to find root node for entity_id
        try:
            parent_comment = Comment.objects.get(entity_type=entity_type, entity_id=entity_id)
        except Comment.DoesNotExist:
            parent_comment = Comment(entity_type=entity_type, entity_id=entity_id)
            parent_comment.save()
    new_comment = Comment(parent=parent_comment, content=content, user=user)
    new_comment.save()
    return HttpResponse('Comment successfully added')


def change_comment(request, comment_id, content):
    try:
        comment = Comment.objects.get(id=comment_id)
        comment.content = content
        comment.save()
        return HttpResponse('Comment successfully changed')
    except Comment.DoesNotExist:
        return HttpResponseNotFound('Comment does not exist')


def delete_comment(request, comment_id):
    try:
        comment = Comment.objects.get(id=comment_id)
        if not comment.is_leaf_node():
            return HttpResponse('Delete of this comment not asseptable', status=406)
        comment.delete()
        return HttpResponse('Comment successfully deleted')
    except Comment.DoesNotExist:
        return HttpResponseNotFound('Comment does not exist')


def get_first_comments(request, entity_type_id, entity_id, page):
    allowed_entities = ['blog_page_id', 'user_page_id', 'comment_id']

    try:
        entity_type = EntityType.objects.get(id=entity_type_id)
    except EntityType.DoesNotExist:
        return HttpResponseNotFound('Entity type does not exist')
    if entity_type.name not in allowed_entities:
        return HttpResponseBadRequest('Entity type not allowed')

    if entity_type.name == 'comment_id':
        root = Comment.objects.get(id=entity_id)
    else:
        root = Comment.objects.get(entity_type=entity_type, entity_id=entity_id)
    comment_queryset = root.get_children()
    paginator = Paginator(comment_queryset, settings.COMMENTS_PER_PAGE)
    comments = paginator.page(page)
    return JsonResponse(comments)


def get_comments(request, entity_type_id, entity_id):
    allowed_entities = ['blog_page_id', 'user_page_id', 'comment_id', 'user_id']

    try:
        entity_type = EntityType.objects.get(id=entity_type_id)
    except EntityType.DoesNotExist:
        return HttpResponseNotFound('Entity type does not exist')
    if entity_type.name not in allowed_entities:
        return HttpResponseBadRequest('Entity type not allowed')

    if entity_type.name == 'user_id':
        try:
            user = User.objects.get(id=entity_id)
        except User.DoesNotExist:
            return HttpResponseNotFound('User does not exist')
        comments = Comment.objects.filter(user=user)
    elif entity_type.name == 'comment_id':
        try:
            root = Comment.objects.get(id=entity_id)
        except Comment.DoesNotExist:
            return HttpResponseNotFound('Root comment does not exist')
        comments = root.get_descendants()
    else:
        try:
            root = Comment.objects.get(entity_type=entity_type, entity_id=entity_id)
        except Comment.DoesNotExist:
            return HttpResponseNotFound('Root object does not exist')
        comments = root.get_descendants()
    return JsonResponse(comments)


def dump_comments_async(request, entity_type_id, entity_id, date_from=None, date_to=None, dump_format='xml'):
    task_id = async('testwork.comments.tasks.fetch_comments', entity_type_id=entity_type_id, entity_id=entity_id,
                    date_from=date_from, date_to=date_to, dump_format=dump_format)
    return JsonResponse({'task_id': task_id})


def task_result(request, task_id):
    data = result(task_id)
    if not data:
        return HttpResponse('Result not ready yet', status=204)
    content_type, comments = data
    if not content_type:
        return HttpResponseBadRequest(comments)
    return HttpResponse(comments, content_type=content_type)


# for debug purpose
def dump_comments_sync(request, entity_type_id, entity_id, date_from=None, date_to=None, dump_format='xml'):
    content_type, comments = fetch_comments(entity_type_id, entity_id, date_from, date_to, dump_format)
    if not content_type:
        return HttpResponseBadRequest(comments)
    return HttpResponse(comments, content_type=content_type)
