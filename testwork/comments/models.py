# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from mptt.models import MPTTModel, TreeForeignKey


class EntityType(models.Model):
    name = models.CharField(max_length=20)
    # TODO: maybe need to add entity model name


class Comment(MPTTModel):
    content = models.CharField(max_length=140)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    user = models.ForeignKey(User, default=None, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    entity_type = models.ForeignKey(EntityType, default=None, null=True)
    entity_id = models.PositiveIntegerField(default=None, null=True)
    # TODO: add property edited when exists history for comment
    # TODO: add property deleted when exists history with action delete for comment

    class MPTTMeta:
        order_insertion_by = ['date_created']

    class Meta:
        index_together = ['entity_type', 'entity_id']


class ChangeType(models.Model):
    name = models.CharField(max_length=20)


class CommentHistory(models.Model):
    comment = models.PositiveIntegerField()
    date_edited = models.DateTimeField(auto_now_add=True)
    change_type = models.ForeignKey(ChangeType)

