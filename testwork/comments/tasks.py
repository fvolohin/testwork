from django.contrib.auth.models import User
from django.core.serializers import serialize

from testwork.comments.models import Comment, EntityType


def get_content_type(dump_format):
    if dump_format == 'yaml':
        return 'text/plain'
    else:
        return 'application/%s' % dump_format


# call as async task
def fetch_comments(entity_type_id, entity_id, date_from, date_to, dump_format):
    allowed_entities = ['blog_page_id', 'user_page_id', 'comment_id', 'user_id']
    allowed_formats = ['xml', 'json', 'yaml']

    if dump_format not in allowed_formats:
        return (None, 'Format not allowed')

    entity_type = EntityType.objects.get(id=entity_type_id)
    if entity_type.name not in allowed_entities:
        return (None, 'Entity type not allowed')

    if entity_type.name == 'user_id':
        try:
            user = User.objects.get(id=entity_id)
        except User.DoesNotExist:
            return (None, 'User does not exist')
        comments = Comment.objects.filter(user=user)
    elif entity_type.name == 'comment_id':
        try:
            root = Comment.objects.get(id=entity_id)
        except Comment.DoesNotExist:
            return (None, 'Root comment does not exist')
        comments = root.get_descendants()
    else:
        try:
            root = Comment.objects.get(entity_type=entity_type, entity_id=entity_id)
        except Comment.DoesNotExist:
            return (None, 'Root object does not exist')
        comments = root.get_descendants()
    return (get_content_type(dump_format), serialize(dump_format, comments))
