"""testwork URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from comments.views import create_comment, change_comment, delete_comment, get_first_comments, get_comments
from comments.views import dump_comments_async, task_result, dump_comments_sync

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^create_comment/(?P<entity_type_id>[0-9]+)/(?P<entity_id>[0-9]+)/(?P<user_id>[0-9]+)/(?P<content>.*)/$',
        create_comment),
    url(r'^change_comment/(?P<comment_id>[0-9]+)/(?P<content>.*)/$',
        change_comment),
    url(r'^delete_comment/(?P<comment_id>[0-9]+)/$',
        delete_comment),
    url(r'^get_first_comments/(?P<entity_type_id>[0-9]+)/(?P<entity_id>[0-9]+)/(?P<page>[0-9]+)/$', get_first_comments),
    url(r'^get_comments/(?P<entity_type_id>[0-9]+)/(?P<entity_id>[0-9]+)/$', get_comments),
    # all time
    url(r'^dump_comments_async/(?P<entity_type_id>[0-9]+)/(?P<entity_id>[0-9]+)/(?P<dump_format>xml|json|yaml)/$',
        dump_comments_async),
    # from date to date
    url(r'^dump_comments_async/(?P<entity_type_id>[0-9]+)/(?P<entity_id>[0-9]+)/(?P<date_from>\d{4}-\d{2}-\d{2})/(?P<date_to>\d{4}-\d{2}-\d{2})/(?P<dump_format>xml|json|yaml)/$',
        dump_comments_async),
    # for test purpose
    url(r'^dump_comments_sync/(?P<entity_type_id>[0-9]+)/(?P<entity_id>[0-9]+)/(?P<dump_format>xml|json|yaml)/$',
        dump_comments_sync),
    url(r'^task_result/(?P<task_id>\w+)/$', task_result),
]

