# testwork.comments

## intro

My main goal was to make working solution which satisfy most of requriements.  
For satisfy performance requirements I used MPTT model and module **django-mptt**. It gives much performance on operation read from tree but need extra work for keep tree in consistant state.  
I use root element of tree (level 0) as link to parent entity (blog or user page).
I use **django_q** module for async task, because it native, easy to implement and similar to Celery. It fits my requirements for now. Using Celery would be more flexible and efficient decision. But more complex at the same time.  
For dump data I use simple django serialize method just to check that solution is work. Better decision would be to use **Django Rest Framework**. It allow to control how we return entities and which attributes we need in response.  
Not satisfied requirement is push-notifications. We can choose **django-notifications** module for this purpose.

First of all I convert requirements from description to tasks. I used **Trello**, board attached to **Bitbucket** project.

Next steps I see:
* Use Django Rest Framework
* Refactor to exclude repeated blocks of code
* Add tests
* Add logging


## init commands

```bash
# for manage.py dbshell working
sudo apt install postgresql
```
```bash
# install python requirements
virtualenv venv
source venv/bin/activate
pip install -r requiremets.txt
```
```bash
# run postgres in docker
docker pull postgres \
docker run --name testwork-postgres -e POSTGRES_PASSWORD=123 -d -p 5432:5432 postgres
```
```bash
# create init tables
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata ./testwork/comments/fixtures/entity_types.json
python manage.py loaddata ./testwork/comments/fixtures/change_types.json
python manage.py loaddata ./testwork/comments/fixtures/user.json
# TODO: add fixtures dir to settings
```
```bash
# run task menagement
python manage.py qcluster
```
